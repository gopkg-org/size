/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package size

import (
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"
	"unicode"
)

const (
	Byte Size = 1 << (iota * 10)
	KibiByte
	MebiByte
	GibiByte
	TebiByte
	PebiByte

	KiloByte = Byte * 1000
	MegaByte = KiloByte * 1000
	GigaByte = MegaByte * 1000
	TeraByte = GigaByte * 1000
	PetaByte = TeraByte * 1000
)

var sizeTable = map[string]Size{
	"": Byte,

	"B":  Byte,
	"kB": KiloByte,
	"MB": MegaByte,
	"GB": GigaByte,
	"TB": TeraByte,
	"PB": PetaByte,

	"kiB": KibiByte,
	"MiB": MebiByte,
	"GiB": GibiByte,
	"TiB": TebiByte,
	"PiB": PebiByte,
}

func ParseToBytes(str string) (uint64, error) {
	str = strings.TrimSpace(str)

	var suffix string
	for i := 0; i < len(str); i++ {
		if unicode.IsLetter(rune(str[i])) {
			suffix += string(str[i])
		}
	}

	value, err := strconv.ParseFloat(strings.TrimSpace(strings.ReplaceAll(strings.TrimSuffix(str, suffix), ",", "")), 64)
	if err != nil {
		return 0, errors.New("invalid value")
	}

	if m, ok := sizeTable[suffix]; ok {
		value *= float64(m)

		if value > math.MaxUint64 {
			return 0, errors.New("too large")
		}

		return uint64(value), nil
	}

	return 0, errors.New("invalid suffix")
}

func Parse(str string) (Size, error) {
	s, err := ParseToBytes(str)
	if err != nil {
		return 0, err
	}

	return Size(s), nil
}

type Size uint64

func (s Size) String() string { return s.SI() }

func (s Size) Bytes() uint64 { return uint64(s) }

func (s Size) KiloBytes() float64 { return float64(s) / float64(KiloByte) }
func (s Size) MegaBytes() float64 { return float64(s) / float64(MegaByte) }
func (s Size) GigaBytes() float64 { return float64(s) / float64(GigaByte) }
func (s Size) TeraBytes() float64 { return float64(s) / float64(TeraByte) }
func (s Size) PetaBytes() float64 { return float64(s) / float64(PetaByte) }

func (s Size) KibiBytes() float64 { return float64(s) / float64(KibiByte) }
func (s Size) MebiBytes() float64 { return float64(s) / float64(MebiByte) }
func (s Size) GibiBytes() float64 { return float64(s) / float64(GibiByte) }
func (s Size) TebiBytes() float64 { return float64(s) / float64(TebiByte) }
func (s Size) PebiBytes() float64 { return float64(s) / float64(PebiByte) }

func (s Size) SI() string  { return s.format(1000, "kB", "MB", "GB", "TB", "PB") }
func (s Size) EIC() string { return s.format(1024, "kiB", "MiB", "GiB", "TiB", "PiB") }

func (s Size) format(base int, suffixes ...string) string {
	if uint64(s) < uint64(base) {
		return fmt.Sprintf("%d B", s)
	}

	e := math.Floor(math.Log(float64(s)) / math.Log(float64(base)))
	v := float64(s) / math.Pow(float64(base), e)

	return fmt.Sprintf("%.2f %s", v, suffixes[int(e)-1])
}
