/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package size_test

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gopkg.org/size"
)

func TestSize(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(SizeSuite))
}

type SizeSuite struct {
	suite.Suite
}

func (s *SizeSuite) TestInvalidValue() {
	res, err := size.Parse("This is a test")

	s.EqualError(err, "invalid value")
	s.Equal(size.Size(0), res)
}

func (s *SizeSuite) TestInvalidSuffix() {
	res, err := size.Parse("1 TT")

	s.EqualError(err, "invalid suffix")
	s.Equal(size.Size(0), res)
}

func (s *SizeSuite) TestTooLarge() {
	res, err := size.Parse("20,000 PB")

	s.EqualError(err, "too large")
	s.Equal(size.Size(0), res)
}

func (s *SizeSuite) TestOneByte() {
	res, err := size.Parse("1")

	s.NoError(err)
	s.Equal(size.Size(1), res)
	s.Equal(uint64(1), res.Bytes())

	s.Equal(0.001, res.KiloBytes())
	s.Equal(0.000001, res.MegaBytes())
	s.Equal(0.000000001, res.GigaBytes())
	s.Equal(0.000000000001, res.TeraBytes())
	s.Equal(0.000000000000001, res.PetaBytes())

	s.Equal(float64(1)/(1<<10), res.KibiBytes())
	s.Equal(float64(1)/(1<<20), res.MebiBytes())
	s.Equal(float64(1)/(1<<30), res.GibiBytes())
	s.Equal(float64(1)/(1<<40), res.TebiBytes())
	s.Equal(float64(1)/(1<<50), res.PebiBytes())

	s.Equal("1 B", res.SI())
	s.Equal("1 B", res.EIC())
}

func (s *SizeSuite) TestOneKiloByte() {
	res, err := size.Parse("1kB")

	s.NoError(err)
	s.Equal(size.Size(1000), res)
	s.Equal(uint64(1000), res.Bytes())

	s.Equal(1.0, res.KiloBytes())
	s.Equal(0.001, res.MegaBytes())
	s.Equal(0.000001, res.GigaBytes())
	s.Equal(0.000000001, res.TeraBytes())
	s.Equal(0.000000000001, res.PetaBytes())

	s.Equal(float64(1000)/(1<<10), res.KibiBytes())
	s.Equal(float64(1000)/(1<<20), res.MebiBytes())
	s.Equal(float64(1000)/(1<<30), res.GibiBytes())
	s.Equal(float64(1000)/(1<<40), res.TebiBytes())
	s.Equal(float64(1000)/(1<<50), res.PebiBytes())

	s.Equal("1.00 kB", res.SI())
	s.Equal("1000 B", res.EIC())
}

func (s *SizeSuite) TestOneMegaByte() {
	res, err := size.Parse("1MB")

	s.NoError(err)
	s.Equal(size.Size(1000000), res)
	s.Equal(uint64(1000000), res.Bytes())

	s.Equal(1000.0, res.KiloBytes())
	s.Equal(1.0, res.MegaBytes())
	s.Equal(0.001, res.GigaBytes())
	s.Equal(0.000001, res.TeraBytes())
	s.Equal(0.000000001, res.PetaBytes())

	s.Equal(float64(1000000)/(1<<10), res.KibiBytes())
	s.Equal(float64(1000000)/(1<<20), res.MebiBytes())
	s.Equal(float64(1000000)/(1<<30), res.GibiBytes())
	s.Equal(float64(1000000)/(1<<40), res.TebiBytes())
	s.Equal(float64(1000000)/(1<<50), res.PebiBytes())

	s.Equal("1.00 MB", res.SI())
	s.Equal("976.56 kiB", res.EIC())
}

func (s *SizeSuite) TestOneGigaByte() {
	res, err := size.Parse("1GB")

	s.NoError(err)
	s.Equal(size.Size(1000000000), res)
	s.Equal(uint64(1000000000), res.Bytes())

	s.Equal(1000000.0, res.KiloBytes())
	s.Equal(1000.0, res.MegaBytes())
	s.Equal(1.0, res.GigaBytes())
	s.Equal(0.001, res.TeraBytes())
	s.Equal(0.000001, res.PetaBytes())

	s.Equal(float64(1000000000)/(1<<10), res.KibiBytes())
	s.Equal(float64(1000000000)/(1<<20), res.MebiBytes())
	s.Equal(float64(1000000000)/(1<<30), res.GibiBytes())
	s.Equal(float64(1000000000)/(1<<40), res.TebiBytes())
	s.Equal(float64(1000000000)/(1<<50), res.PebiBytes())

	s.Equal("1.00 GB", res.SI())
	s.Equal("953.67 MiB", res.EIC())
}

func (s *SizeSuite) TestOneTeraByte() {
	res, err := size.Parse("1TB")

	s.NoError(err)
	s.Equal(size.Size(1000000000000), res)
	s.Equal(uint64(1000000000000), res.Bytes())

	s.Equal(1000000000.0, res.KiloBytes())
	s.Equal(1000000.0, res.MegaBytes())
	s.Equal(1000.0, res.GigaBytes())
	s.Equal(1.0, res.TeraBytes())
	s.Equal(0.001, res.PetaBytes())

	s.Equal(float64(1000000000000)/(1<<10), res.KibiBytes())
	s.Equal(float64(1000000000000)/(1<<20), res.MebiBytes())
	s.Equal(float64(1000000000000)/(1<<30), res.GibiBytes())
	s.Equal(float64(1000000000000)/(1<<40), res.TebiBytes())
	s.Equal(float64(1000000000000)/(1<<50), res.PebiBytes())

	s.Equal("1.00 TB", res.SI())
	s.Equal("931.32 GiB", res.EIC())
}

func (s *SizeSuite) TestOnePetaByte() {
	res, err := size.Parse("1PB")

	s.NoError(err)
	s.Equal(size.Size(1000000000000000), res)
	s.Equal(uint64(1000000000000000), res.Bytes())

	s.Equal(1000000000000.0, res.KiloBytes())
	s.Equal(1000000000.0, res.MegaBytes())
	s.Equal(1000000.0, res.GigaBytes())
	s.Equal(1000.0, res.TeraBytes())
	s.Equal(1.0, res.PetaBytes())

	s.Equal(float64(1000000000000000)/(1<<10), res.KibiBytes())
	s.Equal(float64(1000000000000000)/(1<<20), res.MebiBytes())
	s.Equal(float64(1000000000000000)/(1<<30), res.GibiBytes())
	s.Equal(float64(1000000000000000)/(1<<40), res.TebiBytes())
	s.Equal(float64(1000000000000000)/(1<<50), res.PebiBytes())

	s.Equal("1.00 PB", res.SI())
	s.Equal("909.49 TiB", res.EIC())
}

func (s *SizeSuite) TestOneHundredAndTwentyThreeAndFiftyNineHundredthGigaByte() {
	res, err := size.Parse("123.59 GB")

	s.NoError(err)
	s.Equal(size.Size(123590000000), res)
	s.Equal(uint64(123590000000), res.Bytes())

	s.Equal(123590000.0, res.KiloBytes())
	s.Equal(123590.0, res.MegaBytes())
	s.Equal(123.59, res.GigaBytes())
	s.Equal(0.12359, res.TeraBytes())
	s.Equal(0.00012359, res.PetaBytes())

	s.Equal(float64(123590000000)/(1<<10), res.KibiBytes())
	s.Equal(float64(123590000000)/(1<<20), res.MebiBytes())
	s.Equal(float64(123590000000)/(1<<30), res.GibiBytes())
	s.Equal(float64(123590000000)/(1<<40), res.TebiBytes())
	s.Equal(float64(123590000000)/(1<<50), res.PebiBytes())

	s.Equal("123.59 GB", res.SI())
	s.Equal("115.10 GiB", res.EIC())
}

func (s *SizeSuite) TestMultipleKiloBytes() {
	for val, exp := range map[string]size.Size{
		"148008 kB":  148008000,
		"1601392 kB": 1601392000,
		"0 kB":       0,
		"14192 kB":   14192000,
		"12848 kB":   12848000,
		"3424 kB":    3424000,
		"9424 kB":    9424000,
		"140 kB":     140000,
		"4856 kB":    4856000,
		"1608 kB":    1608000,
		"220 kB":     220000,
	} {
		res, err := size.Parse(val)
		s.NoError(err)
		s.Equal(exp, res)
	}
}

func (s *SizeSuite) TestString() {
	s.Equal("1 B", size.Byte.String())
	s.Equal("1.00 kB", size.KiloByte.String())
	s.Equal("1.00 MB", size.MegaByte.String())
	s.Equal("1.00 GB", size.GigaByte.String())
	s.Equal("1.00 TB", size.TeraByte.String())
	s.Equal("1.00 PB", size.PetaByte.String())
	s.Equal("1.02 kB", size.KibiByte.String())
	s.Equal("1.05 MB", size.MebiByte.String())
	s.Equal("1.07 GB", size.GibiByte.String())
	s.Equal("1.10 TB", size.TebiByte.String())
	s.Equal("1.13 PB", size.PebiByte.String())
}
